<?php

namespace App\Http\Controllers;


use App\Models\Blogi;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BlogiController extends Controller
{
   
    public function index()
    {
        return Inertia::render('Blog');
    }

    
    public function create()
    {
        return Inertia::render('Auth/Blog/Create')
    }

  
    public function store(Request $request)
    {
        Blogi::create($request->validate([
            'title' => 'required',
            'description' => 'required',
        ]));
        return redirect()->back();
    }

  
    
    public function edit(Blogi $blogi)
    {
        return Inertia::render('Auth/Blog/Edit', [
            'post' => $blog
        ]);
    }

   
    public function update(Request $request, Blogi $blogi)
    {
        $blok->update($request->validate([
            'title' => 'required',
            'description' => 'required',
        ]));
        return redirect()->back();
    }

   
    public function destroy(Blogi $blogi)
    {
        $blok->delete();
        return redirect()->back();
    }
}
