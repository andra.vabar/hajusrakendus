<?php

namespace App\Http\Controllers;

use App\Models\Maps;
use Illuminate\Http\Request;

class MapsController extends Controller
{
   
    public function index()
    {
       return Inertia::render('Map');
    }

   
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show(Maps $maps)
    {
        //
    }

   
    public function edit(Maps $maps)
    {
        //
    }

  
    public function update(Request $request, Maps $maps)
    {
        //
    }

    
    public function destroy(Maps $maps)
    {
        //
    }
}
